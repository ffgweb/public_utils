import environ

env = environ.Env()

conf = env.db()

if conf['ENGINE'] == 'django.db.backends.mysql':
    print 'mysql --user={} --password={} --host={} --port={} {}'.format(
        conf['USER'] or 'root',
        conf['PASSWORD'],
        conf['HOST'] or 'localhost',
        conf['PORT'] or '3306',
        conf['NAME']
    )

elif conf['ENGINE'] == 'django.db.backends.postgresql_psycopg2':
    print 'env PGPASSWORD={} psql --host={} --port={} --username={} {}'.format(
        conf['PASSWORD'],
        conf['HOST'] or 'localhost',
        conf['PORT'] or '5432',
        conf['USER'] or 'postgres',
        conf['NAME']
    )

else:
    raise Exception('Unsupported db engine: {}'.format(conf['ENGINE']))
