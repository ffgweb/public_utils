import os
import json
import urllib2
import base64
import time

username = os.environ.get('RANCHER_ACCESS_KEY')
password = os.environ.get('RANCHER_SECRET_KEY')
base_url = os.environ.get('RANCHER_URL')
stack_name = os.environ.get('STACK_NAME')

base64string = base64.encodestring('%s:%s' % (username, password)).replace('\n', '')


def do_request(url, method='GET', payload=None):
    full_url = base_url + url
    kwargs = {}
    if payload:
        kwargs['data'] = json.dumps(payload)

    request = urllib2.Request(full_url, **kwargs)
    request.add_header('Authorization', 'Basic %s' % base64string)
    if payload:
        request.add_header('Content-Type', 'application/json')
    request.get_method = lambda: method
    return json.loads(urllib2.urlopen(request).read())


url = '/stacks/?name=' + stack_name
data = do_request(url)

if len(data['data']) <= 0:
    print('Cannot find stack "%s"' % (stack_name,))
    raise SystemExit

stack_id = data['data'][0]['id']
env_id = data['data'][0]['accountId']

print('Found stack "%s" at env_id=%s and stack_id=%s' % (stack_name, env_id, stack_id,))

data = None
while True:
    # we don't care about infinite loop
    # Jenkins will break this on timeout
    url = '/projects/' + env_id + '/stacks/' + stack_id
    data = do_request(url)

    if data['state'] not in ('upgraded', 'upgrading', 'finishing-upgrade', 'rolling-back',):
        print('Stack is ready to be upgraded')
        break

    print('Waiting for upgrade in progress to finish')

    time.sleep(3)

# get current stack configuration
rancher_compose = data['rancherCompose']
docker_compose = data['dockerCompose']
environment = data['environment']
external_id = data['externalId']

for k in environment:
    varname = 'UPGRADE_' + k
    if os.environ.get(varname):
        old_value = environment[k]
        new_value = os.environ.get(varname)
        environment[k] = new_value
        print('New env var value %s="%s" (old value was "%s"' % (k, new_value, old_value,))

payload = {
    'externalId': external_id,
    'dockerCompose': docker_compose,
    'rancherCompose': rancher_compose,
    'environment': environment,
}

url = '/projects/' + env_id + '/stacks/' + stack_id + '?action=upgrade'
do_request(url, method='POST', payload=payload)

print('Started stack upgrade')

while True:
    # we don't care about infinite loop
    # Jenkins will break this on timeout
    url = '/projects/' + env_id + '/stacks/' + stack_id
    data = do_request(url)

    if data['state'] == 'upgraded':
        print('Stack upgrade ready to finish')
        break

    print('Stack upgrade still in progress')

    time.sleep(3)

url = '/projects/' + env_id + '/stacks/' + stack_id + '?action=finishupgrade'
do_request(url, method='POST')

print('Finished stack upgrade')
