import json


class Entity:
    def __init__(self, full_json, data=None):
        self._data = data or {}
        self._full_json = full_json

    def __getattr__(self, item):
        return self._data.get(item)

    def get_entity(self, entity_class, condition):
        all_entities = self.get_all_json_entities_by_class(entity_class)
        entity_data = next((e for e in all_entities if condition(e)), None)
        if entity_data is None:
            raise EntityNotFoundException(entity_class, condition)

        return entity_class(self._full_json, entity_data)

    def get_entities(self, entity_class, condition=None):
        all_entities = self.get_all_json_entities_by_class(entity_class)
        return (
            entity_class(self._full_json, entity_data)
            for entity_data in all_entities
            if condition is None or condition(entity_data)
        )

    def get_entity_by_pk(self, entity_class, pk_value):
        def condition(entity):
            return entity['pk'] == pk_value

        return self.get_entity(entity_class, condition)

    def get_entities_by_fk(self, entity_class):
        fk_name = self.get_fk_name()

        def condition(entity):
            return entity[fk_name] == self.pk

        return self.get_entities(entity_class, condition)

    def get_related_entity(self, entity_class):
        pk_value = getattr(self, entity_class.get_fk_name())
        return self.get_entity_by_pk(entity_class, pk_value)

    def get_all_json_entities_by_class(self, entity_class):
        mapped_name = entity_class.get_mapped_name()
        return self._full_json['entityGroupMap'][mapped_name]['entities']

    @classmethod
    def get_fk_name(cls):
        return cls.__name__.lower() + '_pk'

    @classmethod
    def get_mapped_name(cls):
        return cls.__name__ + ':#'

    def __repr__(self):
        return '{} pk#{}'.format(self.__class__.__name__, self.pk)


class TournamentResult(Entity):
    @classmethod
    def load_file(cls, path):
        with open(path) as f:
            full_json = json.load(f)
            return cls(full_json)

    @classmethod
    def load_string(cls, string):
        full_json = json.loads(string)
        return cls(full_json)

    def get_tournament(self, pk):
        return self.get_entity_by_pk(Tournament, pk)

    def get_tournaments(self):
        return self.get_entities(Tournament)

    def get_participant(self, pk):
        return self.get_entity_by_pk(Participant, pk)

    def get_participants(self):
        return self.get_entities(Participant)

    def get_match(self, pk):
        return self.get_entity_by_pk(Match, pk)

    def get_matches(self):
        return self.get_entities(Match)

    def get_deck(self, pk):
        return self.get_entity_by_pk(Deck, pk)

    def get_decks(self):
        return self.get_entities(Deck)

    def get_tiebreaker(self, pk):
        return self.get_entity_by_pk(Tiebreaker, pk)

    def get_tiebreakers(self):
        return self.get_entities(Tiebreaker)

    def get_game_setting(self, pk):
        return self.get_entity_by_pk(GameSettings, pk)

    def get_game_settings(self):
        return self.get_entities(GameSettings)


class Deck(Entity):
    def get_participant(self):
        return self.get_related_entity(Participant)

    def __repr__(self):
        return 'Deck {} pk#{}'.format(self.deck_id, self.pk)


class Match(Entity):
    def get_tournament(self):
        return self.get_related_entity(Tournament)

    def get_match_participants(self):
        return self.get_entities_by_fk(MatchParticipant)

    def __repr__(self):
        return 'Match {} round {} pk#{}'.format(self.order_index, self.round, self.pk)


class MatchParticipant(Entity):
    def get_match(self):
        return self.get_related_entity(Match)

    def get_participant(self):
        return self.get_related_entity(Participant)

    def __repr__(self):
        return 'Match pk#{} participant pk#{}'.format(self.match_pk, self.participant_pk)


class Participant(Entity):
    def get_tournament(self):
        return self.get_related_entity(Tournament)

    def get_decks(self):
        return self.get_entities_by_fk(Deck)

    def get_match_participants(self):
        return self.get_entities_by_fk(MatchParticipant)

    def __repr__(self):
        return 'Participant {} {} pk#{}'.format(self.first_name, self.last_name, self.pk)


class Tournament(Entity):
    def get_matches(self):
        return self.get_entities_by_fk(Match)

    def get_participants(self):
        return self.get_entities_by_fk(Participant)

    def __repr__(self):
        return 'Tournament {} pk#{}'.format(self.name, self.pk)


class Tiebreaker(Entity):
    def __repr__(self):
        return 'Tiebreaker {} pk#{}'.format(self.name, self.pk)


class GameSettings(Entity):
    def __repr__(self):
        return 'GameSettings {} pk#{}'.format(self.name, self.pk)


class EntityNotFoundException(Exception):
    def __init__(self, entity_class, condition):
        super(Exception, self).__init__()
        self.entity_class = entity_class
        self.condition = condition
