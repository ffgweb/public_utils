# README #

## rancher/stack_upgrade.py ##

Script to upgrade a stack meant to be run from Jenkinsfile. It gets its parameters from environment.

### Rancher configuration: ###

`RANCHER_ACCESS_KEY` - Rancher API Key

`RANCHER_SECRET_KEY` - Rancher API Secret

`RANCHER_URL` - Rancher API base URL ie. `https://rancher.example.com/v2-beta`

`STACK_NAME` - Stack name

### Stack variables to be updated: ###

Prefix Rancher's answers variables with `UPGRADE_`. All can be ommited. For example:

`UPGRADE_CMS_VERSION` - it's value will be copied to `CMS_VERSION`

